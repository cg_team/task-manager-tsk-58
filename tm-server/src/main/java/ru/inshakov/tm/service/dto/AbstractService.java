package ru.inshakov.tm.service.dto;

import ru.inshakov.tm.api.IService;
import ru.inshakov.tm.dto.AbstractEntity;
import ru.inshakov.tm.service.AbstractEntityService;

public abstract class AbstractService<E extends AbstractEntity> extends AbstractEntityService implements IService<E> {

}
