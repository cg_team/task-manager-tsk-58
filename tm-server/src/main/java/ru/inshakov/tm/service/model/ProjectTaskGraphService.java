package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.repository.model.IProjectGraphRepository;
import ru.inshakov.tm.api.repository.model.ITaskGraphRepository;
import ru.inshakov.tm.api.service.model.IProjectTaskGraphService;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.TaskGraph;
import ru.inshakov.tm.repository.model.ProjectGraphRepository;
import ru.inshakov.tm.repository.model.TaskGraphRepository;

import java.util.List;
import java.util.Optional;

@Service
public final class ProjectTaskGraphService extends AbstractGraphService implements IProjectTaskGraphService {

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskGraph> findTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {

            return repository.findAllTaskByProjectId(userId, projectId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void bindTaskById(
            @NotNull final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();

            repository.bindTaskToProjectById(userId, taskId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskById(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();

            repository.unbindTaskById(userId, taskId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        @NotNull final IProjectGraphRepository projectRepository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }


    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        @NotNull final IProjectGraphRepository projectRepository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByIndex(userId, index))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        @NotNull final IProjectGraphRepository projectRepository = context.getBean(ProjectGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final String projectId = Optional.ofNullable(projectRepository.findByName(userId, name))
                    .orElseThrow(ProjectNotFoundException::new)
                    .getId();
            repository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIdUserId(userId, projectId);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}