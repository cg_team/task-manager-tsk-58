package ru.inshakov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.repository.model.ITaskGraphRepository;
import ru.inshakov.tm.api.service.model.ITaskGraphService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.TaskGraph;
import ru.inshakov.tm.model.UserGraph;
import ru.inshakov.tm.repository.model.TaskGraphRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskGraphService {

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll() {
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            return repository.findAll();
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskGraph> collection) {
        if (collection == null) return;
        for (TaskGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@Nullable final TaskGraph entity) {
        if (entity == null) return null;
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            repository.add(entity);
            repository.getTransaction().commit();
            return entity;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final List<TaskGraph> tasks = repository.findAll();
            for (TaskGraph t :
                    tasks) {
                repository.remove(t);
            }
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final TaskGraph task = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
            if (task == null) return;
            repository.remove(task);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskGraph entity) {
        if (entity == null) return;
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final TaskGraph task = repository.getReference(entity.getId());
            if (task == null) return;
            repository.remove(task);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            return repository.findByIndex(userId, index);
        } finally {
            repository.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            return repository.findByName(userId, name);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final TaskGraph task = repository.findByIndex(userId, index);
            if (task == null) return;
            repository.remove(task);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final TaskGraph task = repository.findByName(userId, name);
            if (task == null) return;
            repository.remove(task);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.IN_PROGRESS);
            task.setStartDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIndex(userId, index))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @NotNull final TaskGraph task = Optional.ofNullable(repository.findByName(userId, name))
                    .orElseThrow(TaskNotFoundException::new);
            task.setStatus(Status.COMPLETED);
            task.setFinishDate(new Date());
            repository.update(task);
            repository.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @SneakyThrows
    @Nullable
    public TaskGraph add(@NotNull UserGraph user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskGraph task = new TaskGraph(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll(@NotNull final String userId) {
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            return repository.findAllByUserId(userId);
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph user, @Nullable final Collection<TaskGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (TaskGraph item : collection) {
            item.setUser(user);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@NotNull final UserGraph user, @Nullable final TaskGraph entity) {
        if (entity == null) return null;
        entity.setUser(user);
        @Nullable final TaskGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            repository.clearByUserId(userId);
            @NotNull final List<TaskGraph> tasks = repository.findAllByUserId(userId);
            for (TaskGraph t :
                    tasks) {
                repository.remove(t);
            }
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final TaskGraph task = repository.findByIdUserId(
                    userId,
                    optionalId.orElseThrow(EmptyIdException::new)
            );
            if (task == null) return;
            repository.remove(task);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskGraph entity) {
        if (entity == null) return;
        @NotNull final ITaskGraphRepository repository = context.getBean(TaskGraphRepository.class);
        try {
            repository.getTransaction().begin();
            @Nullable final TaskGraph task = repository.findByIdUserId(userId, entity.getId());
            if (task == null) return;
            repository.remove(task);
            repository.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            repository.getTransaction().rollback();
            throw e;
        } finally {
            repository.close();
        }
    }

}
