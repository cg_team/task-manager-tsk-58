package ru.inshakov.tm.api.repository.dto;

import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.dto.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    void removeUserByLogin(final String login);

    void update(final User user);


}
