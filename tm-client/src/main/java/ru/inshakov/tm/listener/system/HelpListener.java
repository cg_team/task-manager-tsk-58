package ru.inshakov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AbstractListener;
import ru.inshakov.tm.service.ListenerService;

@Component
public class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    private ListenerService commandService;

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        for (@NotNull final AbstractListener command : commandService.getListeners()) {
            System.out.println(command.toString());
        }
    }
}
