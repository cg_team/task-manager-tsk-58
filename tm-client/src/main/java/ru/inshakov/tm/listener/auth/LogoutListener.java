package ru.inshakov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.inshakov.tm.event.ConsoleEvent;
import ru.inshakov.tm.listener.AuthAbstractListener;
import ru.inshakov.tm.endpoint.SessionEndpoint;

@Component
public class LogoutListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout from to application.";
    }

    @Override
    @EventListener(condition = "@logoutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        sessionEndpoint.close(getSession());
    }
}
