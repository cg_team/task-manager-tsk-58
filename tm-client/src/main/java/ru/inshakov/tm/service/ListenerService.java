package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.inshakov.tm.api.repository.IListenerRepository;
import ru.inshakov.tm.api.service.IListenerService;
import ru.inshakov.tm.listener.AbstractListener;

import java.util.Collection;

@Service
public class ListenerService implements IListenerService {

    @NotNull
    private final IListenerRepository listenerRepository;

    @NotNull
    public ListenerService(@NotNull IListenerRepository listenerRepository) {
        this.listenerRepository = listenerRepository;
    }

    @Nullable
    @Override
    public AbstractListener getListenerByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return listenerRepository.getListenerByName(name);
    }

    @Nullable
    @Override
    public AbstractListener getListenerByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return listenerRepository.getListenerByArg(arg);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getListeners() {
        return listenerRepository.getListeners();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArguments() {
        return listenerRepository.getArguments();
    }

    @NotNull
    @Override
    public Collection<String> getListListenerName() {
        return listenerRepository.getListenerNames();
    }

    @NotNull
    @Override
    public Collection<String> getListListenerArg() {
        return listenerRepository.getListenerArg();
    }

    @Override
    public void add(@Nullable AbstractListener command) {
        if (command == null) return;
        listenerRepository.add(command);
    }


}
