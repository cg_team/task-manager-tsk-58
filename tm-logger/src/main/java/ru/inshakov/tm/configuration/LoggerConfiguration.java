package ru.inshakov.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.inshakov.tm.listener.LogMessageListener;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;

import static ru.inshakov.tm.constant.ActiveMQConst.URL;

@ComponentScan("ru.inshakov.tm")
public class LoggerConfiguration {

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    public MessageListener listener() {
        return new LogMessageListener();
    }

}
